## Général 
1. Des lignes de communication :fibre, cuivre, radio, satellite
2. La vitesse de transmission dépend de la bande passante disponible
3. Liaison physique : ce qui relie l’émetteur et le récepteur 
4. Câble coaxial: Deux conducteurs concentriques en cuivre, Bidirectionnel, Large Bande (modulation)
5. Fibre optique: Fibre en silice ( verre) ou plastique liaisons point à point très rapide de10Gb/s à 100Gb/s, taux d’erreur très faible, faible atténuation, insensible aux perturbations
électromagnétiques, liaisons directes de plusieurs centaines de km.

## Paquet - source de retard
1. Temps de traitement par le nœud : Vérification de la séquence de contrôle d’erreur, choix de la ligne de sortie
2. File d’attente : Temps à attendre sur la ligne de sortie avant transmission, dépend de l’état de congestion du routeur.
3. Temps de transmission: 
4. Temps de propagation :

## Paquet - débit
1. Rythme (bits/unité de temps) auquel les bits sont transférés entre émetteur et récepteur
2. Débit Instantané : Rythme à un instant donnée
3. Débit Moyen : Rythme sur une longue période => (Ds*Dc) / (Ds+Dc)

## Internet: 
1. Une infrastructure qui offre des services aux applications
2. Des services offerts pour programmer des applications

## Internet : organisation fonctionnelle
1. Des protocoles : ils contrôlent l’envoie et la réception des message <br>
2. Les Standards de l’Internet (RFC - IETF) <br>
3. Internet: “Réseau de réseaux”

## Couche
Pourquoi ? 
1. Pour le traitement des systèmes complexes
2. Permet de modéliser, d’identifier les différents éléments d’un système complexe.
- Définition de modèles de référence pour les fonctionnalités de chaque couche
- La modularisation simplifie la maintenance et la mise à jour du système
- Les changements pouvant intervenir sur la manière de rendre le service pour une couche restent transparent pour le reste du système

## Les différentes couches
1. Application : Gère les programmes réseaux
– FTP, SMTP, HTTP, POP, .. 
2. Transport : Gère le transfert des données debout en bout
– TCP, UDP
3. Network : S’occupe du routage des datagrammes de la source vers la destination
– IP, protocoles de routage
4. Liaison : gère le transfert des données entre deux éléments voisins du réseau
– Ethernet, PPP
5. Physique : comment représenter un bit sur le support de transmission ?

## Sécurité 
1. Cryptographie à clé symétrique : la clé de l'expéditeur est identique à celle du destinataire. KA=KB
2. Cryptographie à clé publique : La clé de chiffrement est publique et différente de la clé de déchiffrement qui n'est connue que du destinataire. KA≠ KB

## Algo 
1. Triple DES, version utilisable du DES devenu obsolète : cassage trop rapide. Clés de 112 ou 168 bits. 
2. AES choisit sur concours par le gouvernement américain en 2001 (NSA), clés de 128, 192 ou 256 bits.
3. Camelia : clés de 128, 192 ou 256 bits, plus lent que AES, choisit par le gouvernement Japonais
4. MD5 largement utilisée, définit dans la RFC1321, inventé par Ronald Rivest en 1991. Empreintes sur 128 bits
5. SHA (Secure Hash Algorithm) conçu par la NSA, définit dans FIPS 180-2

## Exemple de merde 
Voitures roulent à 100 km/h. <br> 
Le passage au péage prend 12s (transmission time) :
- Voiture ~ bit; suite de voitures ~ paquet
1. Q: Combien de temps va-t-il se passer avant que la suite de véhicules se présente denouveau devant le 2ème péage

> Le temps de passage de tous les véhiculent au péage est de 12s*10 = 120s = 2min
> Temps pour le dernier véhicule pour atteindre le 2ème péage : 100km/(100km/h)= 1 h
> Réponse : 62 minutes

## Exemple 2
Les voitures roulent maintenant à 1000 km/h. Le péage traite un véhicule en 1 min
1. Q: est ce que les voitures vont atteindre le 2ème péage avant que toutes les voitures soient passées au 1er péage ?
2. Oui! Après 7min (1min + 100/1000*60min) la 1ère voiture arrive au 2ème péage, et 3 voitures sont encore au 1er péage.
3. Le 1er bit d’un paquet peut arriver au 2ème routeur avant que le paquet ne soit complètement émis par le 1er routeur